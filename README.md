# passgen
Generate xkcd passwords and more!

# Coming soon
 - Built-in definitions! The data files are there and everything, I just have to program it.
 - Want something? Let me know in [issues](https://github.com/nonnymoose/passgen/issues)!
